package challenge1;

import java.util.*;

public class Problem1 {
	
	public static void main(String[] args) {
		TreeSet<Integer> ts1 = new TreeSet<>();
		ts1.add(32);
		ts1.add(89);
		ts1.add(76);
		
		TreeSet<Integer> ts2 = new TreeSet<>();
		ts2.add(5);
		ts2.add(10);
		ts2.add(15);
		
		TreeSet<Integer> ts3 = new TreeSet<>();
		ts3.add(11);
		ts3.add(22);
		ts3.add(33);
		
		
		secondMax(3, ts1, ts2, ts3);
		
	}

	/**
	 * prints out second max in each set (since it is a set of 3, second max will always be on index 1.
	 * @param n (number of triples (sets))
	 * @param t (varargs)
	 */
	static void secondMax(int n, TreeSet... t) {
		
		
		for (TreeSet i : t) {
			List<Integer> list = new ArrayList<Integer>(i);
			System.out.println(list.get(1));
		}
		
		
	}
}
